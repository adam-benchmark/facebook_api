<?php

namespace App\Controller\Facebook;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class FacebookMeController extends Controller
{
    private $facebookService;
    private $helper;
    private $fb;

    public function index()
    {
        $this->facebookService = $this->get('facebook_service');

        $this->getParameter(
            $this->get('app_id'),
            $this->getParameter('app_secret'),
            $this->getParameter('graph_version')
        );

        $this->helper = $this->facebookService->getHelper();
        $this->fb = $this->facebookService->getFB();
        $accessToken = $this->facebookService->getFacebookSessionPersistentDataHandler();

        if (!empty($accessToken)) {
            $this->fb->setDefaultAccessToken($accessToken);
            $response = $this->fb->get('/me?fields=id,email,context');

            $user = $response->getGraphUser();

            return $this->render('Facebook/facebook.me.html.twig', [
                'title' => 'Feed Page',
                'response' => $response,
                'user' => $user,
            ]);

        } elseif ($this->helper->getError()) {
            // The user denied the request
            return $this->redirectToRoute('facebook_login');
        }
    }
}