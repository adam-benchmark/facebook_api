<?php

namespace App\Controller\Facebook;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class FacebookLoginController extends Controller
{
    private $facebookService;

    public function index()
    {
        $this->facebookService = $this->get('facebook_service');
        $this->facebookService->init(
            $this->getParameter('app_id'),
            $this->getParameter('app_secret'),
            $this->getParameter('graph_version')
        );
        $loginUrl = $this->facebookService->getLoginUrl();

        return $this->render('Facebook/facebook.login.html.twig', [
            'title' => 'login page',
            'login_url' => $loginUrl,
        ]);
    }
}