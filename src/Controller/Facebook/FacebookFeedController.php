<?php

namespace App\Controller\Facebook;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class FacebookFeedController extends Controller
{
    private $facebookService;
    private $helper;
    private $fb;

    public function index()
    {
        $this->facebookService = $this->get('facebook_service');

        $this->facebookService->init(
            $this->getParameter('app_id'),
            $this->getParameter('app_secret'),
            $this->getParameter('graph_version')
        );

        $this->helper = $this->facebookService->getHelper();
        $this->fb = $this->facebookService->getFB();
        $accessToken = $this->facebookService->getFacebookSessionPersistentDataHandler();

        if (!empty($accessToken)) {
            $this->fb->setDefaultAccessToken($accessToken);

            $response = $this->fb->get('/cocacola/posts');

            $graphNode = $response->getGraphEdge();

            $data = json_decode($response->getBody(), false);
            foreach ($data->data as $v ) {
                $dataMessages[]['id'] = isset($v->id) ? $v->id : '' ;
                $dataMessages[]['message'] = isset($v->message) ? $v->message : '' ;
            }

            return $this->render('Facebook/facebook.feed.html.twig', [
                'title' => 'Feed Page',
                'response' => $response,
                'data' => $data,
                'message' => $dataMessages,
                'graph_node' => $graphNode->getIterator(),
            ]);

        } elseif ($this->helper->getError()) {

            return $this->redirectToRoute('facebook_login');
        }
    }
}