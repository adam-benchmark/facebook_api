<?php
namespace App\Controller\Facebook;

use App\Services\Facebook\FacebookService;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class FacebookController extends Controller
{
    public function index(FacebookService $facebookService) {


        return $this->render('Facebook/facebook.list.html.twig', [
            'title' => 'Hello in Facebook via Symfony4',
        ]);
    }
}