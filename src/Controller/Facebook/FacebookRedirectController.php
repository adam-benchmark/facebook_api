<?php

namespace App\Controller\Facebook;

use Facebook\Exceptions\FacebookResponseException;
use Facebook\Exceptions\FacebookSDKException;
use Facebook\Helpers\FacebookRedirectLoginHelper;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class FacebookRedirectController extends Controller
{
    private $facebookService;

    public function index()
    {
        $this->facebookService = $this->get('facebook_service');
        $this->facebookService->init(
            $this->getParameter('app_id'),
            $this->getParameter('app_secret'),
            $this->getParameter('graph_version')
        );

        /** @var FacebookRedirectLoginHelper $helper */
        $helper = $this->facebookService->getHelper();

        $fb = $this->facebookService->getFB();

        try {
            $accessToken = $this->facebookService->getAccessToken();
        } catch (FacebookResponseException $e) {

            return $this->redirectToRoute('facebook_login', [
                'title' => 'login page<br />' . $e->getMessage(),
            ]);

        } catch (FacebookSDKException $e) {

            return $this->redirectToRoute('facebook_login', [
                'title' => 'login page <br />' . $e->getMessage(),
            ]);
        }

        if (isset($accessToken)) {
            $this->facebookService->setFacebookSessionPersistentDataHandler($accessToken);

            $fb->setDefaultAccessToken($accessToken);

            return $this->render('Facebook/facebook.menu.html.twig', [
                'title' => 'Response Options'
            ]);

        } elseif ($helper->getError()) {
            // The user denied the request
            return $this->redirectToRoute('facebook_login', [
                'title' => 'login page<br/>!Access Denied!',
            ]);
        }

    }
}