<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

class DefaultController extends Controller
{
    const UPPER_TEST = 'TRAINING TASK';
    const SUBJECT = 'Connecting to Facebook API';

    public function index()
    {

        return $this->render('Default/default.html.twig', [
            'title' => self::UPPER_TEST,
            'subject' => self::SUBJECT
        ]);
    }
}