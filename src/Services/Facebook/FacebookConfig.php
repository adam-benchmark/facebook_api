<?php

namespace App\Services\Facebook;

use Facebook\Facebook;
use Facebook\Helpers\FacebookRedirectLoginHelper;
use Facebook\PersistentData\FacebookSessionPersistentDataHandler;

class FacebookConfig
{
    const FACEBOOK_ACCESS_TOKEN = 'facebook_access_token';
    const REDIRECT_URL =  "http://localhost:8000/facebook/redirect/";

    /** @var array */
    private $parameters;
    /** @var FacebookRedirectLoginHelper */
    private $helper;
    /** @var Facebook */
    private $fb;
    /** @var FacebookSessionPersistentDataHandler  */
    private $facebookSessionPersistentDataHandler;

    /** @var string */
    private $appId;
    /** @var string */
    private $appSecret;
    /** @var string */
    private $graphVersion;

    /**
     * @param $appId
     * @param $appSecret
     * @param $graphVersion
     */
    public function initConfig($appId, $appSecret, $graphVersion)
    {
        $this->appId = $appId;
        $this->appSecret = $appSecret;
        $this->graphVersion = $graphVersion;
        $this->resolveSession();
        $this->resolveFacebookInitialize();
    }

    private function resolveFacebookInitialize()
    {
        $this->fb = new Facebook([
            'app_id' => $this->appId,
            'app_secret' => $this->appSecret,
            'default_graph_version' => $this->graphVersion
        ]);

        $this->helper = $this->fb->getRedirectLoginHelper();

        $this->facebookSessionPersistentDataHandler = new FacebookSessionPersistentDataHandler();
    }

    private function resolveSession()
    {
        if (session_status() !== PHP_SESSION_ACTIVE) {
            session_start();
        }
    }

    /**
     * @return FacebookRedirectLoginHelper
     */
    public function getHelper()
    {
        return $this->helper;
    }

    /**
     * @return Facebook
     */
    public function getFb()
    {
        return $this->fb;
    }

    public function getFacebookSessionPersistentDataHandler()
    {
        return $this->facebookSessionPersistentDataHandler->get(FacebookConfig::FACEBOOK_ACCESS_TOKEN);
    }

    public function setFacebookSessionPersistentDataHandler($accessToken)
    {
        $this->facebookSessionPersistentDataHandler->set(
            FacebookConfig::FACEBOOK_ACCESS_TOKEN,
            (string)$accessToken
        );
    }
}