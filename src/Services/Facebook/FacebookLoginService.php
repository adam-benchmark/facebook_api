<?php

namespace App\Services\Facebook;

use Facebook\Helpers\FacebookRedirectLoginHelper;

class FacebookLoginService
{
    /** @var string */
    private $accesToken;
    /** @var FacebookRedirectLoginHelper */
    private $helper;
    /** @var string */
    private $loginUrl;
    /** @var FacebookConfig */
    private $facebookConfig;

    /**
     * FacebookLoginService constructor.
     */
    public function __construct(FacebookConfig $facebookConfig)
    {
        /*$this->facebookConfig = $facebookConfig;*/
    }

    /**
     * @param string $appId
     */
   /* public function init($appId)
    {

        $this->helper = $this->facebookConfig->getHelper();

        $permissions = ['email'];
        $this->loginUrl = $this->helper->getLoginUrl(FacebookConfig::REDIRECT_URL, $permissions);
        $this->setAccessToken();
    }*/

    /**
     * @return string
     */
    /*public function getLoginUrl()
    {
        return $this->loginUrl;
    }*/

   /* private function setAccessToken()
    {
        $this->accessToken = $this->helper->getAccessToken();
    }*/

    /**
     * @return string
     */
    /*public function getAccessToken()
    {
        return $this->accesToken;
    }*/
}