<?php

namespace App\Services\Facebook;

use Facebook\Facebook;
use Facebook\Helpers\FacebookRedirectLoginHelper;

class FacebookService
{
    //https://github.com/facebook/php-graph-sdk
    /** @var Facebook */
    private $fb;
    /** @var FacebookLoginService */
    private $facebookLoginService;
    /** @var FacebookConfig  */
    private $facebookConfig;
    /** @var FacebookRedirectLoginHelper */
    private $helper;
    /** @var string */
    private $loginUrl;
    /** @var string */
    private $accessToken;

    /**
     * FacebookService constructor.
     */
    public function __construct(
        FacebookConfig $facebookConfig,
        FacebookLoginService $facebookLoginService
    )
    {
        $this->facebookConfig = $facebookConfig;
        $this->facebookLoginService = $facebookLoginService;
    }

    /**
     * @return mixed
     */
    public function getLoginUrl()
    {
        return $this->loginUrl;
    }

    /**
     * @param string $appId
     */
    public function init($appId ,$appSecret, $graphVersion)
    {
        $this->facebookConfig->initConfig($appId, $appSecret, $graphVersion);
        $this->fb = $this->facebookConfig->getFb();
        $this->helper = $this->facebookConfig->getHelper();

        $permissions = ['email'];
        $this->loginUrl = $this->helper->getLoginUrl(FacebookConfig::REDIRECT_URL, $permissions);
        $this->accessToken = $this->helper->getAccessToken();
    }

    public function getHelper()
    {
        return $this->helper;
    }

    public function getAccessToken()
    {
        return $this->accessToken;
    }

    public function getFB()
    {
        return $this->fb;
    }

    public function setFacebookSessionPersistentDataHandler($accessToken)
    {
        $this->facebookConfig->setFacebookSessionPersistentDataHandler($accessToken);
       /* $this->facebookSessionPersistentDataHandler->set(
            FacebookConfig::FACEBOOK_ACCESS_TOKEN,
            (string)$accessToken
        );*/
    }

    public function getFacebookSessionPersistentDataHandler()
    {
        return $this->facebookConfig->getFacebookSessionPersistentDataHandler();
    }

}